/**
 * http://www.tutorialspoint.com/java/java_multithreading.htm
 */
public class RunnableDemo implements Runnable {
    private Thread t;
    private String threadName;

    static void log(String msg) {
        System.out.println(msg);
    }

    RunnableDemo (String name) {
        threadName = name;
        log("Creating " + threadName);
    }

    public void run() {
        System.out.println("Running " + threadName);
        try {
            for (int i = 4; i > 0; i--) {
                log("Thread: " + threadName + ", " + i);
                // let thread get sleepy
                Thread.sleep(50);
            }
        } catch (InterruptedException e){
            log("Thread: " + threadName + " interrupted.");
        }
        log("Thread: " + threadName + " exiting.");
    }

    public void start() {
        log("Starting " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
}
